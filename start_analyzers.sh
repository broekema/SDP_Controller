#!/bin/bash

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
host="10.0.0.10"
nr_analyzers=1
output_file=$host-""
verbose=0
base_port=1234
prot='udp'


SSH='ssh'
SSH_OPTS='-n -f'
BASE_DIR='/home/chris/Code/SDP_Controller/'
BIN='analyzer'
PORT=$base_port
OPEN="'"
CLOSE="' &"

function show_help {
    echo "$0 [-h -f [output_file] -n [number] -t [host] -p [port]]"
    echo ""
    echo "-h show help"
    echo "-f [output_file] use 'output_file' as the root file name for redirected output. Padded with analyzer number."
    echo "-n [number] start n analyzers"
    echo "-t [host] use host to start analyzers on"
    echo "-p [port] start at port number p, analyzer number is added"
    echo "-l do not user ssh, but start analyzers locally"
}



while getopts "h?vf:n:t:p:l" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    v)  verbose=1
        ;;
    f)  output_file=$OPTARG
        ;;
    n)  nr_analyzers=$OPTARG
	;;
    t)  host=$OPTARG
	;;
    p)  base_port=$OPTARG
	;;
    l)  SSH=""
	SSH_OPTS=""
	host=""
	OPEN=""
	CLOSE="&"
	output_file=$HOSTNAME"-"
    esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

#echo "verbose=$verbose, output_file_base='$output_file_base', nr_analyzers='$nr_analyzers', host='$host', Leftovers: $@"



for x in $(seq -w 1 $nr_analyzers) ;
do
    echo $SSH $SSH_OPTS $host $OPEN$BASE_DIR$BIN $prot':0.0.0.0:'$PORT '2>' $output_file$x $CLOSE
    PORT=$(($PORT + 1))
done

# End of file



