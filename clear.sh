#!/bin/bash

PYTHONPATH=../ryu python SDP_controller.py -clear
PYTHONPATH=../ryu python SDP_controller.py -add_broadcast
PYTHONPATH=../ryu python SDP_controller.py -l2_rule 1 2
