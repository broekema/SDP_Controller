SDP_controller README
=====================

This is an experimental Software-defined networking application for
radio astronomy. It is intended to interact with a ryu network
controller with a RESTful interface to ofctl running.

Prerequisites
-------------

- A SDN capable switch
- the ryu network controller configured to control the above switch
- a running ryu RESTful interface to ofctl
    $> bin/ryu-manager ryu/app/ofctl_rest.py
- a number of nodes connected to known ports on the switch to send and
  receive data

Content
-------

The main component is SDP_controller.py, which is a command-line
application used to run proof of concept experiments. It contains a
somewhat extensive help screen that explains the main functions of the
program. Several helper scripts have been added to simplify running
the various experiments.

An XML file is used to gather information on the connected hosts. See
hosts.xml for an example.

- generator.c : a data source that mimics a LOFAR station RSP board.
- analyzer.c : a data sink that analyzes the received data for missing
  and out-of-order packets.


Contact information
-------------------

For questions about this software, contact Chris Broekema
broekema <at> astron DOT nl
