#
# SDP_controller.py
#
# Copyright (C) 2016-2017  ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# The SDP_controller is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The SDP_controller is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the SDP_controller. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import sys
import http.client
import json
from threading import Thread
import threading
import time
import socket

import xml.etree.ElementTree as ET

from ryu.lib.packet import ether_types

def read_hosts(hostfile):
    hosts = ET.parse(hostfile)
    root = hosts.getroot()

    # for host in root.findall('host'):
    #     name = host.get('name')
    #     mac = host.get('mac')
    #     ip = host.get('ip')
    #     print name, ip, mac

JSONHeaders = {'Content-type': 'application/json','Accept': 'application/json'}

def create_connection(server, port, data):
    conn = http.client.HTTPConnection(server, port, timeout=10)
    body = json.dumps(data) if data else ''
    return (conn, body)

def request(conn, action, path, body):
    try:
        conn.request(action, path, body, JSONHeaders)
        print(("sending ", body, "at ", time.time()))
    except Exception as e:
        print(e)
        print('failed to connect ot host')
        return False
    else:
        response = conn.getresponse()
        ret = (response.status, response.reason, response.read())
        print(ret)
        conn.close()
        return response.status == 200


# def listening_thread(host):
#     ip = host.split(':')[0]
#     post = int(host.split(':')[1])

#     s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
#     s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#     s.bind((ip, port))

#     while True:
#         s.recv(1296)
#         global t2
#         t2 = time.time() * 1e6
#         break

#     s.close()
#     event.set()


def switches(ip, port):
    # returns the list of all switches which connect to the controller
    path = '/stats/switches'
    conn, body = create_connection(ip, port, None)
    return request(conn, 'GET', path, body)

def clear_flows(ip, port, switch):
    # Clears all flows on the switch
    path = '/stats/flowentry/delete'

    data1 = {
        "dpid":switch,
        "priority":65534
    }

    data2 = {
        "dpid":switch,
        "priority":1
    }
    
    conn, body = create_connection(ip, port, data1)
    request(conn, 'POST', path, body)
    conn, body = create_connection(ip, port, data2)
    return request(conn, 'POST', path, body)

def clear_groups(ip, port, switch):
    # Clears all group entries on the switch
    path = '/stats/groupentry/delete'
    data = {
        "dpid":switch,
        "group_id":1
    }
    conn, body = create_connection(ip, port, data)
    request(conn, 'POST', path, body)
    
    path = '/stats/groupentry/delete'
    data = {
        "dpid":switch,
        "group_id":6
    }
    conn, body = create_connection(ip, port, data)
    return request(conn, 'POST', path, body)


def flows(ip, port, switch):
    # gets all flows on a switch
    path = '/stats/flow/' + switch
    conn, body = create_connection(ip, port, None)
    return request(conn, 'GET', path, body)

def groups(ip, port, switch):
    # gets all groups on a switch
    path = '/stats/group/' + switch
    conn, body = create_connection(ip, port, None)
    return request(conn, 'GET', path, body)

def port_stats(ip, port, switch, port_nr = 0):
    # return port statistics of a switch. If no port is specified, all are returned.
    path = '/stats/port/' + switch
    conn, body = create_connection(ip, port, None)
    if port_nr == 0:
        return request(conn, 'GET', path, body)
    else:
        return request(conn, 'GET', path + '/' + port_nr, body)

def add_broadcast_rule():
    # add a rule to output broadcast packets to all ports
    path = '/stats/flowentry/add'
    flow = {
        "dpid":switch,
        "priority":"1",
        "match":
        {
            "dl_dst": "ff:ff:ff:ff:ff:ff"   # broadcast mac address
        },
        "actions":[
            {
                "type":"OUTPUT",
                "port":"OFPP_ALL"
            }
        ]
    }
    conn, body = create_connection(ip, port, flow)
    request(conn, 'POST', path, body)

    
def add_l2_rule(hostA, hostB):
    # adds a conventional rule to the switch to connect hostA to hostB
    # this will be overridden by higher priority rules later

    # NOTE: this rule is needlessly complicated: there is no need to limit this rule only to P2P
    # i.e.: a rule that simply matches dl_dst to the correct output port would do just as well
    # This is implemented below (l2_entry) and this piece of code is retained for historic reasons.

    root = hosts.getroot()
    h1 = root.findall(".//host[@id='" + hostA + "']")
    h1_ip = h1[0].find ('ip').text
    h1_mac = h1[0].find('mac').text
    h1_port = int(h1[0].find('port').text)
    
    root = hosts.getroot()
    h2 =  root.findall(".//host[@id='" + hostB + "']")
    h2_ip = h2[0].find ('ip').text
    h2_mac = h2[0].find('mac').text
    h2_port = int(h2[0].find('port').text)
    
     ## Now that we have the information, use the RESTful interface to set the flow
    path = '/stats/flowentry/add'
    flow1 = {
        "dpid":switch,
        "priority":"1",
        "match":
        {
            "dl_dst": h1_mac,
            "in_port":h2_port                 
        },
        
        "actions":[
            {
                "type": "OUTPUT",
                "port": h1_port
            }
        ]
    }
    flow2 = {
        "dpid":switch,
        "priority":"1",
        "match":
        {
            "dl_dst": h2_mac,
            "in_port": h1_port
        },
        "actions":[
            {
                "type": "OUTPUT",
                "port": h2_port
            }
        ]
    }
    
    conn, body = create_connection(ip, port, flow1)
    request(conn, 'POST', path, body)
    conn, body = create_connection(ip, port, flow2)
    request(conn, 'POST', path, body)


def l2_entry(hostA):
    root = hosts.getroot()
    h1 = root.findall(".//host[@id='" + hostA + "']")
    h1_ip = h1[0].find ('ip').text
    h1_mac = h1[0].find('mac').text
    h1_port = int(h1[0].find('port').text)
    
    path = '/stats/flowentry/add'
    flow = {
        "dpid":switch,
        "priority":"1",
        "match":
        {
            "dl_dst": h1_mac,
        },
        "actions":[
            {
                "type": "OUTPUT",
                "port": h1_port
            }
        ]
    }
    conn, body = create_connection(ip, port, flow)
    return request(conn, 'POST', path, body)

def redirect(hostA, hostB):
    # gather host information from xml file
    root = hosts.getroot()
    h1 = root.findall(".//host[@id='" + hostA + "']")
    h1_ip = h1[0].find ('ip').text
    h1_mac = h1[0].find('mac').text
    h1_port = int(h1[0].find('port').text)
    
    root = hosts.getroot()
    h2 =  root.findall(".//host[@id='" + hostB + "']")
    h2_ip = h2[0].find ('ip').text
    h2_mac = h2[0].find('mac').text
    h2_port = int(h2[0].find('port').text)
    
    # print(h1, h1_ip, h1_mac, h1_port)
    # print(h2, h2_ip, h2_mac, h2_port)

    ## Now that we have the information, use the RESTful interface to set the flow
    path = '/stats/flowentry/add'

    flow = {
        "dpid":switch,
        "priority":"65534",
        "match":
        {
            "eth_type": ether_types.ETH_TYPE_IP,    # Need to set ether type when matching L3 fields
            "ipv4_dst": h1_ip
                 
        },
        
        "actions":[
            {
                "type": "SET_FIELD",
                "field": "ipv4_dst",
                "value": h2_ip
            },
            {
                "type": "SET_FIELD",
                "field": "eth_dst",
                "value": h2_mac
            },
            {
                "type": "OUTPUT",
                "port": h2_port
            }
        ]
    }
    
    conn, body = create_connection(ip, port, flow)
    return request(conn, 'POST', path, body)
    

def l2_redirect(hostA, hostB):
    # redirect data destined for A to B using L2 information only
    # note that destination IP address is not changed, so host B must have
    # the same IP address as host A
    
    # gather host information first
    root = hosts.getroot()
    h1 = root.findall(".//host[@id='" + hostA + "']")
    h1_ip = h1[0].find ('ip').text
    h1_mac = h1[0].find('mac').text
    h1_port = int(h1[0].find('port').text)
    
    root = hosts.getroot()
    h2 =  root.findall(".//host[@id='" + hostB + "']")
    h2_ip = h2[0].find ('ip').text
    h2_mac = h2[0].find('mac').text
    h2_port = int(h2[0].find('port').text)
    
    path = '/stats/flowentry/add'
    
    flow = {
        "dpid":switch,
        "priority":"65534",
        "match":
        {
            "eth_dst": h1_mac
                 
        },
        
        "actions":[
            {
                "type": "SET_FIELD",
                "field": "eth_dst",
                "value": h2_mac
            },
            {
                "type": "OUTPUT",
                "port": h2_port
            }
        ]
    }
    conn, body = create_connection(ip, port, flow)
    return request(conn, 'POST', path, body)


def duplicate(hostA, hostB, groupID=1):
    # duplicate data destined for hostA to hostB
    # Get relevant information from xml file
    root = hosts.getroot()
    h1 = root.findall(".//host[@id='" + hostA + "']")
    h1_ip = h1[0].find ('ip').text
    h1_mac = h1[0].find('mac').text
    h1_port = int(h1[0].find('port').text)
    
    root = hosts.getroot()
    h2 =  root.findall(".//host[@id='" + hostB + "']")
    h2_ip = h2[0].find ('ip').text
    h2_mac = h2[0].find('mac').text
    h2_port = int(h2[0].find('port').text)

    # print(h1, h1_ip, h1_mac, h1_port)
    # print(h2, h2_ip, h2_mac, h2_port)


    # First install a flow group entry to define what we want to do with the packets
    path = '/stats/groupentry/add'
    flow = {
        "dpid":switch,
        "type":"ALL",
        "group_id":1,
        "buckets":[
            {
                "actions": [
                    {
                        "type": "SET_FIELD",
                        "field": "ipv4_dst",
                        "value": h2_ip
                    },
                    {
                        "type": "SET_FIELD",
                        "field": "eth_dst",
                        "value": h2_mac
                    },
                    {
                        "type": "OUTPUT",
                        "port": h2_port
                    }
                ]

            },
            {
                "actions" : [
                    {
                        "type": "OUTPUT",
                        "port": h1_port
                    }
                ]
            }
        ]
    }
    conn, body = create_connection(ip, port, flow)
    request(conn, 'POST', path, body)

    # And then install a flow that matches packets destined for hostA and apply the previously defined flow group
    path = '/stats/flowentry/add'
    flow = {
        "dpid":switch,
        "priority":"65534",
        "match":
        {
            "eth_type": ether_types.ETH_TYPE_IP,    # need to set ether type when matching L3 fields
            "ipv4_dst": h1_ip
        },
        "actions": [
            {
                "type":"GROUP",
                "group_id":groupID
            }
        ]
    }
    conn, body = create_connection(ip, port, flow)
    return request(conn, 'POST', path, body)


def l2_duplicate(hostA, hostB, groupID=1):
    # duplicate data destined for hostA to hostB
    # Get relevant information from xml file
    root = hosts.getroot()
    h1 = root.findall(".//host[@id='" + hostA + "']")
    h1_mac = h1[0].find('mac').text
    h1_port = int(h1[0].find('port').text)
    
    root = hosts.getroot()
    h2 =  root.findall(".//host[@id='" + hostB + "']")
    h2_mac = h2[0].find('mac').text
    h2_port = int(h2[0].find('port').text)

    path = '/stats/groupentry/add'
    flow = {
        "dpid":switch,
        "type":"ALL",
        "group_id":1,
        "buckets":[
            {
                "actions": [
                    {
                        "type": "SET_FIELD",
                        "field": "eth_dst",
                        "value": h2_mac
                    },
                    {
                        "type": "OUTPUT",
                        "port": h2_port
                    }
                ]

            },
            {
                "actions" : [
                    {
                        "type": "OUTPUT",
                        "port": h1_port
                    }
                ]
            }
        ]
    }
    conn, body = create_connection(ip, port, flow)
    request(conn, 'POST', path, body)

    # And then install a flow that matches packets destined for hostA and apply the previously defined flow group
    path = '/stats/flowentry/add'
    flow = {
        "dpid":switch,
        "priority":"65534",
        "match":
        {
            "eth_dst": h1_mac
        },
        "actions": [
            {
                "type":"GROUP",
                "group_id":groupID
            }
        ]
    }
    conn, body = create_connection(ip, port, flow)
    return request(conn, 'POST', path, body)



def add_broadcast_group():
    # add a OpenFlow group that broadcasts packets with the broadcast MAC address to all ports (except input port)
    
    # fist create a broadcast group
    path = '/stats/groupentry/add'
    flow = {
        "dpid":switch,
        "type":"ALL",
        "group_id":6,
        "buckets":[
            {
                "actions": [
                    {
                        "type": "OUTPUT",
                        "port": 65    ## OFPP_ALL, OFPP_FLOOD, OFPP_NORMAL do not work on SN2100
                    }
                ]
            }
        ]
    }
    conn, body = create_connection(ip, port, flow)
    request(conn, 'POST', path, body)

    # then install a flow that matches broadcast packets to the installed group
    path = '/stats/flowentry/add'
    flow = {
        "dpid":switch,
        "priority":"65534",
        "match":
        {
            "eth_dst": "ff:ff:ff:ff:ff:ff"
        },
        "actions": [
            {
                "type":"GROUP",
                "group_id":6
            }
        ]
    }
    conn, body = create_connection(ip, port, flow)
    return request(conn, 'POST', path, body)


def l2_broadcast(hostA, hostB):    
    root = hosts.getroot()
    h1 = root.findall(".//host[@id='" + hostA + "']")
    h1_mac = h1[0].find('mac').text
    h1_port = int(h1[0].find('port').text)
    
    root = hosts.getroot()
    h2 =  root.findall(".//host[@id='" + hostB + "']")
    h2_mac = h2[0].find('mac').text
    h2_port = int(h2[0].find('port').text)

    ## Now that we have the information, use the RESTful interface to set the flow
    path = '/stats/flowentry/add'
    flow1 = {
        "dpid":switch,
        "priority":"2",
        "match":
        {
            "dl_dst": h1_mac,
        },
        "actions":[
            {
                "type": "SET_FIELD",
                "field": "eth_dst",
                "value": "FF:FF:FF:FF:FF:FF"
            },
            {
                "type": "OUTPUT",
                "port": h1_port
            },
            {
                "type": "OUTPUT",
                "port": h2_port
            }
        ]
    }
    
    conn, body = create_connection(ip, port, flow1)
    return request(conn, 'POST', path, body)

def add_to_group(hostA):
    root = hosts.getroot()
    h1 = root.findall(".//host[@id='" + hostA + "']")
    h1_ip = h1[0].find ('ip').text
    h1_mac = h1[0].find('mac').text
    h1_port = int(h1[0].find('port').text)

    flow = {
       "dpid":switch,
        "type":"ALL",
        "group_id":1,
        "buckets":[
            {
                "actions": [
                    {
                        "type": "SET_FIELD",
                        "field": "ipv4_dst",
                        "value": h1_ip
                    },
                    {
                        "type": "SET_FIELD",
                        "field": "eth_dst",
                        "value": h1_mac
                    },
                    {
                        "type": "OUTPUT",
                        "port": h1_port
                    }
                ]

            }
        ]
    }
    path = '/stats/groupentry/modify'

    conn, body = create_connection(ip, port, flow)
    return request(conn, 'POST', path, body)
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Test SDN feature set for SKA SDP use')
    parser.add_argument('-hosts', help='xml file containing hosts description', default='hosts.xml')
    parser.add_argument('-controller', help='IP address of the network controller', default='localhost')
    parser.add_argument('-cport', help='network controller port to connect to', default='8080')
    parser.add_argument('-switch', help='dpid of the OpenFlow switch', default='1')
    parser.add_argument('-clear', help='clear the currently set flows and groups on the switch', action='store_true')
    parser.add_argument('-dump', help='dump the currently installed flows and groups on the switch', action='store_true')
    parser.add_argument('-dumpflows', help='dump the currently installed flows on the switch', action='store_true')
    parser.add_argument('-dumpgroups', help='dump the currently installed groups on the switch', action='store_true')
    parser.add_argument('-redirect', nargs=2, metavar=('A', 'B'), help='add a flow that redirects data from destination host A to destination host B. Use host ids as defined in the hosts xml file.')
    parser.add_argument('-duplicate', nargs=2, metavar=('A','B'), help='add a flow that duplicates data destined for A to B. Use host ids as defined in the hosts xml file.')
    parser.add_argument('-add_to_group', nargs=1, metavar=('A'), help='add host A to OpenFlow group that duplicates data. Net effect is that data is also duplicated to host A')
    parser.add_argument('-l2_rule', nargs=2, metavar=('A', 'B'), help='add a L2 switch rule to connect host A to host B. Use host ids as defined in the hosts xml file. This rule is overridden by other rules')
    parser.add_argument('-add_broadcast', help='add a L2 broadcast rule to aid ARP discovery', action='store_true')
    parser.add_argument('-add_broadcast_group', help='add a OpenFlow group based L2 broadcast rule to air ARP discovery', action='store_true')
    parser.add_argument('-l2_redirect', nargs=2, metavar=('A','B'), help='add a flow that redirects data from A to B using L2 (MAC address) manipulation only.')
    parser.add_argument('-l2_duplicate', nargs=2, metavar=('A','B'), help='add a flow that duplicates data destined for A to B using L2 (MAC address) manipulation only.')  
    parser.add_argument('-l2_broadcast', nargs=2, metavar=('A','B'), help='add a flow that replaces dst MAC with the broadcast address and forwards packets to hosts A and B')
    
    parser.add_argument('-l2_entry', nargs=1, metavar=('A'), help='add a flow that directs data destined for host A to the correct switch port')



    
    args = parser.parse_args(sys.argv[1:])
    hosts = ET.parse(args.hosts)
    root = hosts.getroot()

    ip = args.controller
    switch = args.switch
    port = args.cport
    
    if (args.clear):
        clear_flows(args.controller, args.cport, args.switch)
        clear_groups(args.controller, args.cport, args.switch)
    elif (args.dump):
        flows(args.controller, args.cport, args.switch)
        groups(args.controller, args.cport, args.switch)
    elif (args.dumpflows):
        flows(args.controller, args.cport, args.switch)
    elif (args.dumpgroups):
        groups(args.controller, args.cport, args.switch)
    elif (args.redirect):
        redirect(args.redirect[0], args.redirect[1])
    elif (args.duplicate):
        duplicate(args.duplicate[0], args.duplicate[1])
    elif (args.add_to_group):
        add_to_group(args.add_to_group[0])
    elif (args.l2_rule):
        add_l2_rule(args.l2_rule[0], args.l2_rule[1])
    elif (args.add_broadcast):
        add_broadcast_rule()
    elif (args.add_broadcast_group):
        add_broadcast_group()
    elif (args.l2_redirect):
        l2_redirect(args.l2_redirect[0], args.l2_redirect[1])
    elif (args.l2_duplicate):
        l2_duplicate(args.l2_duplicate[0], args.l2_duplicate[1])
    elif (args.l2_broadcast):
        l2_broadcast(args.l2_broadcast[0], args.l2_broadcast[1])
    elif (args.l2_entry):
        l2_entry(args.l2_entry[0])
        
