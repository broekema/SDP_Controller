#!/bin/bash

PYTHONPATH=../ryu python SDP_controller.py -switch 0x00007cfe90fe8ac0 -clear
# Broadcast doesn't work on SN2100 
# PYTHONPATH=../ryu python SDP_controller.py -switch 0x00007cfe90fe8ac0 -add_broadcast
PYTHONPATH=../ryu python SDP_controller.py -switch 0x00007cfe90fe8ac0 -hosts hosts_astron.xml -l2_entry 1
PYTHONPATH=../ryu python SDP_controller.py -switch 0x00007cfe90fe8ac0 -hosts hosts_astron.xml -l2_entry 2
PYTHONPATH=../ryu python SDP_controller.py -switch 0x00007cfe90fe8ac0 -hosts hosts_astron.xml -l2_entry 3
PYTHONPATH=../ryu python SDP_controller.py -switch 0x00007cfe90fe8ac0 -hosts hosts_astron.xml -l2_entry 4


