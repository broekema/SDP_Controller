//#
//# analyzer.c
//#
//# Copyright (C) 2012-2018  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the SDP_controller.
//# The SDP_controller is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The SDP_controller is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the SDP_controller. If not, see <http://www.gnu.org/licenses/>.
//#

#include "common.h"

#include <byteswap.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>

#include <pthread.h>

#include <values.h>
#include <string.h>

double   rate		   = 195312.5;
unsigned subbands	   = 61;
unsigned samples_per_frame = 16;
unsigned bits_per_sample   = 16;

char	 name[64];
unsigned message_size;
int	 sk;
unsigned packets_received, missed, out_of_order, errors, bad_timestamps;
unsigned earliest_seconds, earliest_fraction;
unsigned clock_speed;
unsigned long long expected_time;
unsigned long long first_packet_timestamp, last_packet_timestamp;
struct timeval first_packet_arrival, last_packet_arrival;

char multicast_interface[64];
char multicast_addr[64];
unsigned multicast=0;


int receive_packet()
{
  char packet[9000];
  size_t bytes_received;
  unsigned long long time = 0;


  for (bytes_received = 0; bytes_received < message_size;) {
    ssize_t retval = read(sk, packet + bytes_received, message_size - bytes_received);

    if (retval < 0) {
      ++ errors;
      perror("read");
      sleep(1);
    } else if (retval == 0) {
      return 0;
    } else {
      bytes_received += retval;
    }
  }

  ++ packets_received;

#if defined __BIG_ENDIAN__
  unsigned seconds  = __bswap_32(* (int *) (packet +  8));
  unsigned fraction = __bswap_32(* (int *) (packet + 12));
#else
  unsigned seconds  = * (int *) (packet +  8);
  unsigned fraction = * (int *) (packet + 12);
#endif

  if (seconds == 0xFFFFFFFF) {
    ++ bad_timestamps;
  } else {
    time = ((unsigned long long) seconds * clock_speed + 512) / 1024 + fraction;

    if (expected_time != time && expected_time != 0)
      missed += (time - expected_time) / samples_per_frame;

    if (time < expected_time)
      ++ out_of_order;

    expected_time = time + samples_per_frame;
  }

  if (time < first_packet_timestamp) {
    first_packet_timestamp = time;
    gettimeofday(&first_packet_arrival, NULL);
  }
  if (time > last_packet_timestamp) {
    last_packet_timestamp = time;
    gettimeofday(&last_packet_arrival, NULL);
  }

  return 1;
}

void parse_args(int argc, char **argv)
{
  if (argc == 1) {
    fprintf(stderr, "usage: %s [-f frequency (default 195312.5)] [-s subbands (default 61)] [-t times_per_frame (default 16)] [-m multicast_interface (requires -n)] [-n multicast_address (requires -m)] [udp:ip:port | tcp:ip:port | file:name | null: | - ] ... \n", argv[0]);
    exit(1);
  }

  int arg;

  for (arg = 1; arg < argc && argv[arg][0] == '-'; arg ++)
    switch (argv[arg][1]) {
      case 'a': set_affinity(argument(&arg, argv));
		break;

      case 'b': bits_per_sample = atoi(argument(&arg, argv));
		break;

      case 'f': rate = atof(argument(&arg, argv));
		break;

      case 'r': set_real_time_priority();
		break;

      case 's': subbands = atoi(argument(&arg, argv));
		break;

      case 't': samples_per_frame = atoi(argument(&arg, argv));
		break;
      case 'm':	memcpy(&multicast_interface, argument(&arg, argv), 64);
	        break;
      case 'n':	memcpy(&multicast_addr, argument(&arg, argv), 64);
         	multicast=1;
	        break;
      default : fprintf(stderr, "unrecognized option '%c'\n", argv[arg][1]);
	exit(1);
    }

  if (arg == argc)
    exit(0);

  enum proto proto;

  if (multicast) set_multicast(multicast_interface, multicast_addr);

  
  
  sk = create_fd(argv[arg], 0, &proto, name, sizeof name);
}


void print_message()
{
  fprintf(stderr, "received %u packets from %s", packets_received, name);
  if (first_packet_timestamp != MAXLONG) 
    fprintf(stderr, "  first %llu received at %ld.%ld", first_packet_timestamp, first_packet_arrival.tv_sec, first_packet_arrival.tv_usec);
  if (last_packet_timestamp != 0) 
    fprintf(stderr, "  last %llu  received at %ld.%ld", last_packet_timestamp, last_packet_arrival.tv_sec, last_packet_arrival.tv_usec);


  if (missed > 0)
    fprintf(stderr, ", missed = %u", missed);

  if (out_of_order > 0)
    fprintf(stderr, ", out of order = %u", out_of_order);

  if (errors > 0)
    fprintf(stderr, ", read errors = %u", errors);

  if (bad_timestamps > 0)
    fprintf(stderr, ", bad timestamps = %u", bad_timestamps);

  fputc('\n', stderr);
  packets_received = missed = out_of_order = errors = bad_timestamps = 0;
}

void *log_thread(void *arg)
{

  while(1) {
    sleep(1);
    print_message();
  }  
  return 0;
}



int main(int argc, char **argv)
{
  setvbuf(stderr, 0, _IOLBF, 0);
  parse_args(argc, argv);

  message_size = 16 + samples_per_frame * subbands * bits_per_sample / 2;
  clock_speed  = (unsigned) (1024 * rate);

  time_t last_time;
  first_packet_timestamp = MAXLONG;
  last_packet_timestamp = 0;

  
  pthread_t thread;
  if (pthread_create(&thread, 0, log_thread, 0) != 0) {
    perror("pthread_create");
    exit(1);
  }

  while (receive_packet()) {
    time_t current_wtime = time(0);

    /* if (current_wtime != last_time) { */
    /*   last_time = current_wtime; */
    /*   print_message(); */
    /* } */
  }

  print_message();
  return 0;
}

 
