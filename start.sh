#!/bin/bash
TERM=/usr/bin/terminator
OPTS='-b -x'

X1AST=1200
Y1AST=19

X1TH=2560
Y1TH=0

if [ $# -eq 1 ] ; then 
    if [ $1 == "home" ] ; then 
	x1=$X1TH
	y1=$Y1TH
    elif [ $1 == "astron" ] ; then 
	x1=$X1AST
	y1=$Y1AST
    elif [ $1 == "help" ] ; then
	echo "$0 [ astron | home | help ]"
	exit
    else
	echo "unknown parameter name"
    fi
else
    echo "$0 [ astron | home | help ]"
    exit
fi

x2=$(($x1+814))
x3=$(($x2+814))
x4=$(($x3+814))

y2=$(($y1+517))
y3=$(($y2+517))
y4=$(($y3+517))

echo $x4
echo $y2

# Cummulus01
$TERM --geometry=814x480+$x1+$y1   $OPTS ssh mellanox &
$TERM --geometry=814x480+$x1+$y2   $OPTS ssh cummulus01 ./start-ryu-ofctl.sh &
$TERM --geometry=814x480+$x1+$y3   $OPTS ssh cummulus01 &
$TERM --geometry=814x480+$x1+$y4   $OPTS ssh cummulus01 &

# Cummulus02
$TERM --geometry=814x480+$x2+$y1  $OPTS ssh cummulus02 &
$TERM --geometry=814x480+$x2+$y2  $OPTS ssh cummulus02 &
$TERM --geometry=814x480+$x2+$y3  $OPTS ssh cummulus02 &

# Cummulus03
$TERM --geometry=814x480+$x3+$y1  $OPTS ssh cummulus03 &
$TERM --geometry=814x480+$x3+$y2  $OPTS ssh cummulus03 &
$TERM --geometry=814x480+$x3+$y3  $OPTS ssh cummulus03 &

# Cummulus04
$TERM --geometry=814x480+$x4+$y1  $OPTS ssh cummulus04 &
$TERM --geometry=814x480+$x4+$y2  $OPTS ssh cummulus04 &
$TERM --geometry=814x480+$x4+$y3  $OPTS ssh cummulus04 &
