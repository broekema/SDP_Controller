CFLAGS =	-D _DEFAULT_SOURCE -O2 -std=c99 -g

all::		generator analyzer

generator:	generator.c common.c
		gcc $(CFLAGS) $^ -o $@ -lpthread -lm

analyzer:	analyzer.c common.c
		gcc $(CFLAGS) $^ -o $@ -lpthread

clean:
		rm -vf analyzer generator *~
